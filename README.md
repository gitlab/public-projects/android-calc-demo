# Android Calc Demo

This is a very simple calculator app for Android based on the code at
https://github.com/projectworldsofficial/Android-Calculator-App-Project-Source-Code

It provides:
- Android lifecycle alerts
- An example of button handling
- Simple calulations based on buttons